const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');

const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};

let allProject = [];

app.ws('/paint', (ws, req) => {
  const id = nanoid();
  console.log('client connected id = ' + id);
  activeConnections[id] = ws;

  ws.send(JSON.stringify({type: 'DRAW', image: allProject}));


  ws.on('message', msg => {
    const decode = JSON.parse(msg);

    if (decode.type === 'DRAW') {

      allProject.push(...decode.image);

      Object.keys(activeConnections).forEach(key => {
        if (key !== id) {
          const connection = activeConnections[key];
          connection.send(JSON.stringify(decode));
        }
      });
    }
  });

  ws.on('close', () => {
    console.log('client disconnected id = ', id);
    delete activeConnections[id];

    if (Object.keys(activeConnections).length === 0) {
      allProject = [];
    }
  });

});


app.listen(port, () => {
  console.log('Server started on port ' + port);
});

