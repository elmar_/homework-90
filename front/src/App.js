import React, {useEffect, useRef, useState} from 'react';


const App = () => {
  const [state, setState] = useState([]);
  const [mouseDown, setMouseDown] = useState(false);
  const [draw, setDraw] = useState([]);
  const ws = useRef(null);


  const canvas = useRef(null);

  useEffect(() => {
    ws.current = new WebSocket('ws://localhost:8000/paint');

    ws.current.onmessage = event => {
      const decode = JSON.parse(event.data);

      if (decode.type === 'DRAW') {
        setDraw(decode.image);
      }
    };

  }, []);


  useEffect(() => {
    const asyncFo = async () => {
      await makeDraw();
    };

    asyncFo();
  }, [draw]);

  const makeDraw = async () => {
    const context = canvas.current?.getContext('2d');
    if (draw.length === 0) return;
    const copyDraw = [...draw];
    const timer = await setInterval(() => {
      if (copyDraw.length === 0) {
        context.beginPath();
        setDraw([]);

        clearInterval(timer);
        return 0;

      } else {
        const coords = copyDraw.shift();
        const {x, y} = coords;
        const width = 2;
        context.lineWidth = width * 2;

        context.lineTo(x, y);
        context.stroke();

        context.beginPath();
        context.arc(x, y, width, 0, 2 * Math.PI);
        context.fill();

        context.beginPath();
        context.moveTo(x, y);
      }
    }, 0);
    context.beginPath();
  };

  const canvasMouseMoveHandler = event => {
    if (mouseDown) {
      const clientX = event.clientX;
      const clientY = event.clientY;
      setState(prevState => (
        [...prevState, {
          x: clientX,
          y: clientY
        }]));

      const context = canvas.current.getContext('2d');
      const width = 2;
      context.lineWidth = width * 2;


      context.lineTo(clientX, clientY);
      context.stroke();

      context.beginPath();
      context.arc(clientX, clientY, width, 0, 2 * Math.PI);
      context.fill();

      context.beginPath();
      context.moveTo(clientX, clientY);
    }
  };



  const mouseDownHandler = event => {
    setMouseDown(true)
    const context = canvas.current.getContext('2d');
    context.beginPath();
  };


  const mouseUpHandler = event => {
    const context = canvas.current.getContext('2d');
    context.beginPath();
    ws.current.send(JSON.stringify({type: 'DRAW', image: state}));
    setMouseDown(false);
    setState([]);
  };



  return (
    <div>
      <canvas
        ref={canvas}
        style={{border: '1px solid black', display: 'block'}}
        width={window.innerWidth - 18}
        height={window.innerHeight - 18}
        onMouseDown={mouseDownHandler}
        onMouseUp={mouseUpHandler}
        onMouseMove={canvasMouseMoveHandler}
      />
    </div>
  );
};
export default App;
